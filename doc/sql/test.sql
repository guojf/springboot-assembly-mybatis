/*
 Navicat Premium Data Transfer

 Source Server         : 本机
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 08/03/2020 01:26:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for app_demo
-- ----------------------------
DROP TABLE IF EXISTS `app_demo`;
CREATE TABLE `app_demo` (
  `id` varchar(100) COLLATE utf8_bin NOT NULL,
  `nick_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of app_demo
-- ----------------------------
BEGIN;
INSERT INTO `app_demo` VALUES ('1111', '测试数据1');
INSERT INTO `app_demo` VALUES ('2222', '测试数据2');
INSERT INTO `app_demo` VALUES ('3333', '测试数据3');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
