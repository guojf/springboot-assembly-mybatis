# springboot项目打包后,resources,lib分离,保留jar方式运行,mybatis,加模板引擎都是亲测可用的

#### 项目解决默认springboot打成jar执行的一些问题
- 让springboot的项目可以打补丁升级
- 让springboot的resources文件夹独立打包到jar外面同级目录
- 让mybatis的xml不在包装在jar里
- 让那些springboot模板引擎开发的同事不会因为改2行js打一次jar发布
- 总之,resources文件全在外面,你想怎么玩就怎么玩
- 但是,你自己工程的java代码,目前还是打在jar里的,自行学习maven-assembly-plugin插件打到外面也行
### 打包后的截图
![打包后的截图1](doc/img/pkg-info.png)
### 插件打包vs传统打包
![插件打包vs传统打包](doc/img/pkg-comp.png)
### 运行截图
![运行截图](doc/img/run.png)

#### 项目介绍
因为作者js开发的技术菜的抠脚,一晚上升级发布十几次jar包,经常被运维吐槽,每次改2页面行代码,发布一个几十兆的包,然后有幸学习了maven-assembly-plugin
把默认打包方式更改了,这样的话,我修改页面,只需要把那个页面的代码覆盖上去即可,不需要每次都重新打一次jar包,这样保留了jar的启动,也可以跟war包一样解压部署

#### springboot默认的jar痛点
````
在开发springboot项目如果默认的时候打成jar包启动非常方便,但是所有的资源全部打包在一个可执行jar里面
在日常开发部署的时候,经常有时候发布后想要修改配置文件的,遇到这样子的需求,要不重新打个jar,要不就是重新用覆盖配置文件,
非常不方便,尤其是使用模板引擎开发的时候,我们每次页面上可能少了个逗号,或者加几行注释,要不然就是部分的资源需要替换,都非常不方便
这样说吧 只要涉及resources目录下的改动   --几乎重新打全量包,几乎很多人都遇到这蛋疼问题,这个项目就是为了解决这问题搭建的架子
````
#### 软件架构
技术 | 名称 | 官网
----|------|----
Spring  | 容器  | [http://projects.spring.io/spring-framework/](http://projects.spring.io/spring-framework/)
Spring boot | boot官网  | [https://spring.io/projects/spring-boot/](https://spring.io/projects/spring-boot/)
Druid | 数据库连接池  | [https://github.com/alibaba/druid](https://github.com/alibaba/druid)
FreeMarker | 模板引擎  | [http://freemarker.foofun.cn/](http://freemarker.foofun.cn/)
MyBatis-Plus | ORM框架  | [https://mp.baomidou.com/](https://mp.baomidou.com/) 

####软件需求
````
JDK1.8
MySQL5.7
Maven3.0+
````
#### 安装教程--记得去创建数据库文件
````
1. 准备开发环境 jdk8,mysql5.7,maven3
2. 克隆工程 https://gitee.com/chenshiyun/springboot-assembly-mybatis.git  
3. idea,eclipse导入项目
4. 去doc里面复制sql文件去创建数据库表信息,数据库名字test
5. 去application.yaml修改你的数据库配置信息,地址,账号,密码,数据库名称,记得把表导入
````
#### 使用说明
````
1. 启动项目
2. 测试模板引擎是否可以访问到   http://localhost:8080/     对应目录resources/templates/index.ftl
3. 测试静态资源是否可以访问到   http://localhost:8080/web  对应目录resources/static/index.html
4. 测试数据库接口扫描mapper    http://localhost:8080/demo/list  会调用mapper文件的方法,测试mybatis部署
5. 测试数据库接口不扫描mapper  http://localhost:8080/demo/list  这个调用mybatis-plus默认查询
````
#### 打包说明
### idea-maven-插件打包
![idea-maven-插件打包](doc/img/package.png)
````
1. 打包完成后复制target/dist下面的所有文件夹复制去发布即可
2. linux 黑洞启动守护进程启动 nohup java -jar maven-assembly-mybatis-1.0-SNAPSHOT.jar >/dev/null 2>&1 &
3. 其它系统测试的话直接   java -jar maven-assembly-mybatis-1.0-SNAPSHOT.jar 吧
4. 如果改过任何文件,都需要重启
````


#### 学习地址

1. springboot-maven插件 [https://docs.spring.io/spring-boot/docs/current/maven-plugin/](https://docs.spring.io/spring-boot/docs/current/maven-plugin/)
2. Apache Maven Assembly Plugin [http://maven.apache.org/plugins/maven-assembly-plugin/](http://maven.apache.org/plugins/maven-assembly-plugin/)
