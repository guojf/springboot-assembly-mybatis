<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <div>
        <h1>Hello web freeMarker</h1>
        <h1>欢迎访问
            maven-assembly-mybatis 版本,针对springboot打包后,lib,resources分离,照样保留可运行jar包
        </h1>
        <br>
        <a href=" http://localhost:8080/"> 测试模板引擎是否可以访问到</a>        对应目录resources/templates/index.ftl
        <br>
        <a href="http://localhost:8080/web"> 测试静态资源是否可以访问到</a>     对应目录resources/static/index.html
        <br>
        <a href="http://localhost:8080/demo/list"> 测试数据库接口扫描mapper</a>会调用mapper文件的方法,测试mybatis部署
        <br>
        <a href="http://localhost:8080/demo/list">测试数据库接口不扫描mapper</a>    这个调用mybatis-plus默认查询
        <br>
        <a href="https://gitee.com/chenshiyun/springboot-assembly-mybatis">代码码云地址首页</a>
    </div>
</body>
</html>