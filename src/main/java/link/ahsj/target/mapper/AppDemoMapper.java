package link.ahsj.target.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.ahsj.target.entity.AppDemo;

import java.util.List;

public interface AppDemoMapper extends BaseMapper<AppDemo> {

    List<AppDemo> getMapperXmlList();
}
