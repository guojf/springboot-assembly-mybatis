package link.ahsj.target.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.ahsj.target.entity.AppDemo;

import java.util.List;


public interface AppDemoService extends IService<AppDemo> {

    List<AppDemo> getMapperXmlList();

}
