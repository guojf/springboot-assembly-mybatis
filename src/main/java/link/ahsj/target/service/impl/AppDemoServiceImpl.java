package link.ahsj.target.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.ahsj.target.entity.AppDemo;
import link.ahsj.target.mapper.AppDemoMapper;
import link.ahsj.target.service.AppDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AppDemoServiceImpl extends ServiceImpl<AppDemoMapper, AppDemo> implements AppDemoService {

    @Autowired
    private AppDemoMapper appDemoMapper;


    @Override
    public List<AppDemo> getMapperXmlList() {
        return appDemoMapper.getMapperXmlList();
    }
}
