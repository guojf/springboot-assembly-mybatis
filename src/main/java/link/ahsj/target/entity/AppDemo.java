package link.ahsj.target.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @author Eastern unbeaten
 * @email chenshiyun2011@163.com
 * @data 2019-06-22
 */
@TableName("app_demo")
public class AppDemo {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String nickName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
