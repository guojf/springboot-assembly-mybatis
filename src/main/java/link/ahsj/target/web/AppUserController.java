package link.ahsj.target.web;


import link.ahsj.target.service.AppDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/demo")
public class AppUserController {


    @Autowired
    private AppDemoService appDemoService;


    /**
     * 访问mapper.xml文件查询的接口
     * @return
     */
    @GetMapping(value = "/list")
    public ResponseEntity<Object> list() {
        return ResponseEntity.ok(appDemoService.getMapperXmlList());
    }


    /**
     * 调用 mybatis-plus 的查询,无xml的
     * @return
     */
    @GetMapping(value = "/list1")
    public ResponseEntity<Object> list2() {
        return ResponseEntity.ok(appDemoService.list());
    }



}
