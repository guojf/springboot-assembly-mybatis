package link.ahsj.target.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Eastern unbeaten
 * @email chenshiyun2011@163.com
 * @data 2019/5/28
 */
@Controller
public class WebController {


    /**
     * 访问templates下资源
     * @return
     */
    @RequestMapping("/")
    public String hello() {
        return "index";
    }

    /**
     * 访问静态资源
     * @return
     */
    @RequestMapping("/web")
    public String index() {
        return "index.html";
    }
}
